import Foundation

@objc
class TPI_SwiftPluginExample: NSObject, THOPluginProtocol
{
    let activeChannels: [String] = ["lewdbot", "#lewdbot"]
    
    @objc
    let subscribedServerInputCommands = ["privmsg"]
    
    @objc
    func didReceiveServerInput(_ inputObject: THOPluginDidReceiveServerInputConcreteObject, on client: IRCClient)
    {
        switch (inputObject.messageCommand) {
        case "PRIVMSG":
            handleIncomingPrivateMessageCommand(inputObject, on: client)
        default:
            return
        }
    }
    
    func handleIncomingPrivateMessageCommand(_ inputObject: THOPluginDidReceiveServerInputConcreteObject, on client: IRCClient)
    {
        let messageReceived = inputObject.messageSequence
        let messageParamaters = inputObject.messageParamaters
        let senderChannel = client.findChannel(messageParamaters[0])
        
        // we don't accept private messages
        if (senderChannel?.isChannel != true) {
            return
        }
        
        guard let channelName = senderChannel?.name else {
            return
        }
        
        if (!activeChannels.contains(channelName)) {
            return
        }
        
        // is it a duck?
        if (messageReceived == "\\_o< quack!")
        {
            /* Invoke the client on the main thread when sending. */
            performBlock(onMainThread: {
                client.sendPrivmsg("@bang", to: senderChannel!)
            })
        }
        
        // did we miss the duck?
        if (messageReceived.range(of: "you missed the duck!") != nil) {
            performBlock(onMainThread: {
                client.sendPrivmsg("@bang", to: senderChannel!)
            })
        }
    }
}
