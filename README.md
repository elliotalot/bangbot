# bangbot

This is a little demo script to illustrate how to automate behaviour for the Textual IRC client. It can be built with XCode, and injected into Textual as a plugin. It has access too all server communication, and can respond to events. In this case, it waits for a duck in the #lewdbot channel, and responds by killing the duck.

# instructions

Open project with the Textual IRC Client installed in the Applications folder and build. Next, right click the build result and select Textual as the application to open the file in order to install it.

# license

The contents of this repository is released into the public domain for unlimited distribution and modification.